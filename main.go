package main

// docker pull chromedp/headless-shell:latest
// docker run -d -p 9222:9222 --rm --name headless-shell chromedp/headless-shell

import (
	"context"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"strings"
	"sync/atomic"
	"time"

	"github.com/chromedp/chromedp"
)

const selector = "#page-body > div > div.center-content > p:nth-child(2) > span"
const sucessText = "El teu vot ha estat registrat"

var goodResults = int32(0)
var badResults = int32(0)

func main() {
	url := flag.String("url", "", "HTTP(s) url")
	routines := flag.Int("routines", 7, "parallel routines to run")
	remote := flag.String("remote", "", "set remote chromedp instance URL to connect with")
	timeout := flag.Int("timeout", 15, "number of seconds to wait before considering the connection attempt failed")
	flag.Parse()
	init := time.Now()
	for i := 0; i < *routines; i++ {
		go open(*url, *remote, *timeout, i)
	}

	for {
		good := atomic.LoadInt32(&goodResults)
		bad := atomic.LoadInt32(&badResults)
		if good+bad > 0 {
			log.Printf("Good:%d Bad:%d | Sucess ratio: %d%% | Ops/second: %f", good, bad, good*100/(good+bad), float64(good+bad)/time.Since(init).Seconds())
		}
		time.Sleep(time.Second * 5)
	}
}

func open(url string, remote string, timeout int, delta int) {
	log.Println("creating new browser")

	// create main browser context
	mainctx, cancelmain := chromedp.NewContext(context.Background(), chromedp.WithLogf(logs), chromedp.WithBrowserOption(chromedp.WithBrowserErrorf(logs)))
	defer cancelmain()

	// if remote, create a initial context with the remote connection allocated
	ctxremote := context.Background()
	var cancelremote context.CancelFunc
	if remote != "" {
		ctxremote, cancelremote = chromedp.NewRemoteAllocator(mainctx, remote)
		defer cancelremote()
		log.Printf("connecting to remote chromedp %s\n", remote)
	} else {
		// if no remote, open local browser
		if err := chromedp.Run(mainctx); err != nil {
			log.Fatalf("cannot open browser: %s", err)
		}
	}

	var buf []byte
	var output string
	i := 0
	for {
		cdpActions := []chromedp.Action{
			chromedp.Navigate(url),
			chromedp.WaitVisible(selector),
			chromedp.OuterHTML(selector, &output),
		}
		if i%64 == 0 {
			cdpActions = append(cdpActions, chromedp.CaptureScreenshot(&buf))
		}

		// create tab context
		tctx, cancel1 := context.WithTimeout(ctxremote, time.Duration(int(time.Second)*timeout))
		ctx, cancel2 := chromedp.NewContext(tctx)
		// run actions
		err := chromedp.Run(ctx, cdpActions...)
		// check success
		if err == nil {
			if strings.Contains(output, sucessText) {
				atomic.AddInt32(&goodResults, 1)
			} else {
				atomic.AddInt32(&badResults, 1)
				log.Printf("selector content do not match (%s)", output)
			}
		} else {
			atomic.AddInt32(&badResults, 1)
			log.Printf("error: %s\n", err)
		}
		cancel1()
		cancel2()
		if i%64 == 0 && len(buf) > 0 {
			if err := ioutil.WriteFile(fmt.Sprintf("screenshot%d.png", delta), buf, 0644); err != nil {
				log.Printf("warning: cannot save screenshot\n")
			} else {
				log.Printf("taking screenshot, saving on screenshot%d.png", delta)
			}
		}
		i++
	}

}

func logs(s string, i ...interface{}) {
	log.Printf("Console: %s", i)
}
